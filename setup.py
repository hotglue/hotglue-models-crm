#!/usr/bin/env python

from setuptools import setup

setup(
    name="hotglue-models-crm",
    version="0.0.9",
    description="hotglue model definition for CRM",
    author="hotglue",
    url="https://hotglue.xyz",
    classifiers=["Programming Language :: Python :: 3 :: Only"],
    install_requires=[
        "pydantic==1.9.0",
        "typing_extensions>=4.0.0",
        "simplejson"
    ],
    packages=["hotglue_models_crm"],
)
