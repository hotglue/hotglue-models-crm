from datetime import date, datetime, timezone
from typing import List, Optional, Any, ClassVar
from typing_extensions import Literal
from pydantic import BaseModel, validator, Field

def convert_datetime_to_iso_8601(dt: datetime) -> str:
    return dt.isoformat(timespec="milliseconds")


def convert_date_str(dt: datetime.date) -> str:
    return dt.strftime("%Y-%m-%d")


class Address(BaseModel):
    id: Optional[str]
    line1: Optional[str]
    line2: Optional[str]
    line3: Optional[str]
    city: Optional[str]
    state: Optional[str]
    postal_code: Optional[str]
    country: Optional[str]


class Email(BaseModel):
    id: Optional[str]
    email: Optional[str]
    type: Optional[str]


class PhoneNumber(BaseModel):
    number: Optional[str]
    type: Optional[str]


class Attendees(BaseModel):
    id: Optional[str]
    type: Optional[str]


class CallDetails(BaseModel):
    recording_url: Optional[str]
    meeting_url: Optional[str]
    to_number: Optional[str]
    from_number: Optional[str]

class EmailDetails(BaseModel):
    from_email: Optional[str]
    to_emails: Optional[List[str]]
    cc: Optional[List[str]]
    bcc: Optional[List[str]]


class CustomField(BaseModel):
    name: Optional[str]
    label: Optional[str]
    value: Optional[Any]
    type: Optional[str]

class ExternalId(BaseModel):
    name: Optional[str]
    value: Optional[str]

class Campaign(BaseModel):
    id: Optional[str]
    name: Optional[str]
    status:Optional[str]
    start_date:Optional[str]
    end_date:Optional[str]
    type: Optional[str]
    custom_fields: Optional[List[CustomField]]
    active: Optional[bool]

    class Stream:
        name = "Campaigns"

class Contact(BaseModel):
    schema_name: ClassVar[str] = "Contacts"
    id: Optional[str]
    name: Optional[str]
    email: Optional[str]
    unsubscribed: Optional[bool]
    subscribe_status: Optional[str]
    owner_id: Optional[str]
    owner_email: Optional[str]
    owner_name: Optional[str]
    external_id: Optional[ExternalId]
    type: Optional[str]
    company_id: Optional[str]
    company_name: Optional[str]
    first_name: Optional[str]
    middle_name: Optional[str]
    last_name: Optional[str]
    title: Optional[str]
    salutation: Optional[str]
    birthdate: Optional[date]
    number_of_employees: Optional[int]
    website: Optional[str]
    industry: Optional[str]
    rating: Optional[str]
    annual_revenue: Optional[float]
    department: Optional[str]
    photo_url: Optional[str]
    lead_source: Optional[str]
    description: Optional[str]
    status: Optional[str]
    active: Optional[bool]
    addresses: Optional[List[Address]]
    phone_numbers: Optional[List[PhoneNumber]]
    custom_fields: Optional[List[CustomField]]
    updated_at: Optional[datetime]
    created_at: Optional[datetime]
    lists: Optional[List[str]]
    additional_emails: Optional[List[str]]
    campaigns: Optional[List[Campaign]]
    tags: Optional[List[str]]

    class Stream:
        name = "Contacts"

    @validator("updated_at", "created_at")
    @classmethod
    def validate_all_fields_one_by_one(cls, field_value):
        return field_value.astimezone(tz=timezone.utc)

    class Config:
        json_encoders = {
            datetime: convert_datetime_to_iso_8601,
            date: convert_date_str
        }


class Contacts(BaseModel):
    __root__: List[Contact]


class Deal(BaseModel):
    schema_name: ClassVar[str] = "Deals"
    id: Optional[str]
    external_id: Optional[ExternalId]
    title: Optional[str] = Field(description="Title of the deal")
    description: Optional[str] = Field(description="Description of this deal")
    type: Optional[str] = Field(description="Type of the deal. Defined by the user of CRM.")
    monetary_amount: Optional[float]
    currency: Optional[str]
    win_probability: Optional[float]
    expected_revenue: Optional[float]
    close_date: Optional[datetime]
    expected_close_date: Optional[datetime]
    loss_reason_id: Optional[str]
    loss_reason: Optional[str]
    won_reason_id: Optional[str]
    won_reason: Optional[str]
    pipeline_id: Optional[str] = Field(description="The id of the pipeline this deal is in")
    pipeline: Optional[str] = Field(description="Name of the pipeline this deal is in")
    pipeline_stage_id: Optional[str]
    source_id: Optional[str]
    lead_source: Optional[str]
    contact_id: Optional[str]
    contact_email: Optional[str]
    contact_external_id: Optional[ExternalId]
    company_id: Optional[str] = Field(description="The id of the company this deal is for")
    company_name: Optional[str] = Field(description="The name of the company this deal is for")
    priority: Optional[str]
    status: Optional[str]
    owner_id: Optional[str]
    owner_name: Optional[str]
    owner_email: Optional[str]
    status_id: Optional[str] = Field(description="Deal status id in the pipeline.")
    custom_fields: Optional[List[CustomField]]
    active: Optional[bool]
    updated_at: Optional[datetime]
    created_at: Optional[datetime]
    lists: Optional[List[str]] = Field(description="Array of list ids this deal is part of.")

    class Stream:
        name = "Deals"

    @validator("close_date", "updated_at", "created_at")
    @classmethod
    def validate_all_fields_one_by_one(cls, field_value):
        return field_value.astimezone(tz=timezone.utc)

    class Config:
        json_encoders = {
            datetime: convert_datetime_to_iso_8601,
            date: convert_date_str
        }

class Deals(BaseModel):
    __root__: List[Deal]


class Company(BaseModel):
    schema_name: ClassVar[str] = "Companies"
    id: Optional[str]
    name: Optional[str]
    owner_id: Optional[str]
    owner_name: Optional[str]
    owner_email: Optional[str]
    image: Optional[str]
    description: Optional[str]
    status: Optional[str]
    industry: Optional[str]
    website: Optional[str]
    addresses: Optional[List[Address]]
    phone_numbers: Optional[List[PhoneNumber]]
    custom_fields: Optional[List[CustomField]]
    active: Optional[bool]
    updated_at: Optional[datetime]
    created_at: Optional[datetime]
    type: Optional[str]
    social_links: Optional[List[str]]
    source: Optional[str]
    campaign: Optional[str]
    date_acquired: Optional[datetime]
    at_competitor: Optional[bool]

    class Stream:
        name = "Companies"

    @validator("updated_at", "created_at")
    @classmethod
    def validate_all_fields_one_by_one(cls, field_value):
        return field_value.astimezone(tz=timezone.utc)

    class Config:
        json_encoders = {
            datetime: convert_datetime_to_iso_8601,
            date: convert_date_str
        }

class Companies(BaseModel):
    __root__: List[Company]


class Activity(BaseModel):
    schema_name: ClassVar[str] = "Activities"
    id: Optional[str]
    activity_datetime: Optional[datetime]
    duration_seconds: Optional[int]
    contact_id: Optional[str]
    contact_email: Optional[str]
    company_id: Optional[str]
    related_to: Optional[str]
    deal_id: Optional[str]
    owner_id: Optional[str]
    owner_name: Optional[str]
    owner_email: Optional[str]
    type: Optional[str]
    title: Optional[str]
    description: Optional[str]
    note: Optional[str]
    location: Optional[str]
    location_address: Optional[Address]
    active: Optional[bool]
    status: Optional[str]
    start_datetime: Optional[datetime]
    end_datetime: Optional[datetime]
    custom_fields: Optional[List[CustomField]]
    updated_at: Optional[datetime]
    created_at: Optional[datetime]
    recording_url: Optional[str]
    call_details: Optional[CallDetails]
    email_details: Optional[EmailDetails]

    @validator("start_datetime", "end_datetime", "updated_at", "created_at")
    @classmethod
    def validate_all_fields_one_by_one(cls, field_value):
        return field_value.astimezone(tz=timezone.utc)

    class Config:
        json_encoders = {
            datetime: convert_datetime_to_iso_8601,
            date: convert_date_str
        }

    class Stream:
        name = "Activities"

class Activities(BaseModel):
    __root__: List[Activity]


class ListCRM(BaseModel):
    schema_name: ClassVar[str] = "Lists"
    id: Optional[str] = Field(description="Id of the list")
    name: Optional[str] = Field(description="Name of the list")
    type: Optional[str] = Field(description="Deal, Company, or Contact")
    size: Optional[int] = Field(description="Size of the list")
    active: Optional[bool]
    updated_at: Optional[datetime]
    created_at: Optional[datetime]

    @validator("updated_at", "created_at")
    @classmethod
    def validate_all_fields_one_by_one(cls, field_value):
        return field_value.astimezone(tz=timezone.utc)

    class Config:
        json_encoders = {
            datetime: convert_datetime_to_iso_8601,
            date: convert_date_str
        }


class ListsCRM(BaseModel):
    __root__: List[ListCRM]


class Form(BaseModel):
    schema_name: ClassVar[str] = "Forms"
    id: Optional[str]
    name: Optional[str]
    active: Optional[bool]
    updated_at: Optional[datetime]
    created_at: Optional[datetime]

    @validator("updated_at", "created_at")
    @classmethod
    def validate_all_fields_one_by_one(cls, field_value):
        return field_value.astimezone(tz=timezone.utc)

    class Config:
        json_encoders = {
            datetime: convert_datetime_to_iso_8601,
            date: convert_date_str
        }


class Forms(BaseModel):
    __root__: List[Form]


class User(BaseModel):
    schema_name: ClassVar[str] = "Users"
    id: Optional[str]
    first_name: Optional[str]
    last_name: Optional[str]
    email: Optional[str]
    phone: Optional[str]
    active: Optional[bool]
    created_at: Optional[datetime]
    updated_at: Optional[datetime]

    @validator("updated_at", "created_at")
    @classmethod
    def validate_all_fields_one_by_one(cls, field_value):
        return field_value.astimezone(tz=timezone.utc)

    class Config:
        json_encoders = {
            datetime: convert_datetime_to_iso_8601,
            date: convert_date_str
        }

    class Stream:
        name = "Users"

class Users(BaseModel):
    __root__: List[User]


class Note(BaseModel):
    id: Optional[str]
    schema_name: ClassVar[str] = 'Notes'
    company_id: Optional[str]
    company_name: Optional[str]
    deal_id: Optional[str]
    deal_name: Optional[str]
    customer_id: Optional[str]
    customer_name: Optional[str]
    created_at: Optional[datetime]
    updated_at: Optional[datetime]
    content: str

    class Stream:
        name = "Notes"


class Notes(BaseModel):
    __root__: List[Note]

    class Stream:
        name = "Notes"

class Pipeline(BaseModel):
    id: Optional[str]
    name: Optional[str]
    active: Optional[bool]
    created_at: Optional[datetime]
    updated_at: Optional[datetime]

    class Config:
        json_encoders = {
            datetime: convert_datetime_to_iso_8601,
            date: convert_date_str
        }

    class Stream:
        name = "Pipelines"

class Pipelines(BaseModel):
    __root__: List[Pipeline]

class Stage(BaseModel):
    id: Optional[str]
    updated_at: Optional[datetime]
    created_at: Optional[datetime]
    name: Optional[str]
    active: Optional[bool]
    pipeline_id: Optional[str]
    pipeline_name: Optional[str]
    win_probability: Optional[int]

    class Config:
        json_encoders = {
            datetime: convert_datetime_to_iso_8601,
            date: convert_date_str
        }

    class Stream:
        name = "Stages"

class Stages(BaseModel):
    __root__: List[Stage]


class ProductPrice(BaseModel):
    currency: Optional[str]
    price: Optional[float]
    cost: Optional[float]


class Product(BaseModel):
    id: Optional[str]
    name: Optional[str]
    sku: Optional[str]
    description: Optional[str]
    price: Optional[float]
    prices: Optional[List[ProductPrice]]
    cost: Optional[float]
    quantity: Optional[float]
    unit: Optional[float]
    tax: Optional[float]
    category: Optional[str]
    is_active: Optional[bool] = True
    created_at: Optional[datetime]
    updated_at: Optional[datetime]

    class Stream:
        name = "Product"


class Products(BaseModel):
    __root__: List[Product]

    class Stream:
        name = "Products"